﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
@author: Param Randhawa
*/

"use strict";

var _ = require('lodash'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    mincss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer');

var pathCss = './src/assets/css';
var pathJs = './src/assets/js';
var pathFonts = './src/assets/fonts';
var pathSass = './src/assets/sass/**/*.scss';

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

var css = [];

var fonts = [
    './node_modules/font-awesome/fonts/*.*'
];

gulp.task('copy-css', function () {
    _.forEach(css, function (file, _) {
        gulp.src(file)
            .pipe(gulp.dest(pathCss))
    });
    _.forEach(fonts, function (file, _) {
        gulp.src(file)
            .pipe(gulp.dest(pathFonts))
    });
});

gulp.task('copy-min-css', function () {
    _.forEach(css, function (file, _) {
        gulp.src(file)
            .pipe(mincss())
            .pipe(rename({ extname: '.min.css' }))
            .pipe(gulp.dest(pathCss))
    });
    _.forEach(fonts, function (file, _) {
        gulp.src(file)
            .pipe(gulp.dest(pathFonts))
    });
});

gulp.task('sass', function () {
    gulp.src(pathSass)
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(pathCss))
});

gulp.task('sass-min', function () {
    gulp.src(pathSass)
        //.pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer())
        //.pipe(sourcemaps.write())
        .pipe(mincss())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest(pathCss))
});

gulp.task('_watch', function () {
    gulp.watch(pathSass, ['sass'])
        .on('change', function (event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        });
});


gulp.task('_default', ['copy-js', 'copy-css', 'sass']);
gulp.task('_minify', ['copy-min-js', 'copy-min-css', 'sass-min']);